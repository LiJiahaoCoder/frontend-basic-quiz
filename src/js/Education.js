export default class Education {
  constructor(year, title, description) {
    this._year = year;
    this._title = title;
    this._description = description;
  }

  getYear() {
    return this._year;
  }
  getTitle() {
    return this._title;
  }
  getDescription() {
    return this._description;
  }
}
