import $ from 'jquery';

import Person from './Person';
// utils
import fetchData from './util/fetchData';
import render from './util/render';

fetchData
  .then(result => {
    const { name, age, description, educations } = result;
    const person = new Person(name, age, description, educations);
    render(person);
  })
  .catch(error => new Error(error));
