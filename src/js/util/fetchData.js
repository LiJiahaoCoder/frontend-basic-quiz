import { URL } from './data';

export default fetch(URL).then(result => result.json());
